package GUI;
import javax.swing.*;

import Logic.MyAction;

import java.awt.*;

/**
 * ������ ����� ������� �� ����������� ������������ ����������
 * @author Dmitry Byankin
 */
public class GUI 
{
    // ������� ����� - ����, ������� ����� ����������� ������������� � ��������� 
    private JFrame frame;
    
    private JPanel panelField, panelControls;
   
    // ������ ����� ����, ������, ������ � �������� �������� ����
    private JButton newB, quitButton, buttons[];
   
    // isXturn ���������� ��� ����� ('X' - true, 'O' - false)
    // isGameFinished - true, ���� ���� ��������
    // isAITurn - true, ���� ����� ���������
    private boolean isXturn, isGameFinished, isAITurn;
   
    // 
    private int numberOfBoxesLeft, playerMode;


    //����� ������������ ��������� ��� ������� ������
    private MyAction action;

    /**
     * ����������� ������, � ��� ���������� ������������� ���������� � ��������������� �������
     */
    public GUI() {


        frame = new JFrame("��������-������");

        panelField = new JPanel();
        panelControls = new JPanel();

        // ������� ������ ����� ���� � ������
        newB = new JButton("�����");
        quitButton = new JButton("�����");

        // ���� ��� ���� �� ������
        buttons = new JButton[10];
        for (int i = 0; i < 10; i++) 
        {
            buttons[i] = new JButton("");
        }

        // � ����� ������
        isXturn = true;
        isGameFinished = false;
        numberOfBoxesLeft = 9;
        
        
        //�������������� ����� ����������, �� �������� �� ������ ������
        action = new MyAction(this);

        // layout ���������� ��� ������ ��������� ����, ��������� ������
        frame.setLayout(new BorderLayout());
        frame.add(panelField, BorderLayout.CENTER);
        frame.add(panelControls, BorderLayout.SOUTH);

        // ������ - ����� 3�3
        panelField.setLayout(new GridLayout(3, 3));
        for (int i = 1; i < 10; i++) 
        {
        	//��������� ������ �� ������
        	panelField.add(buttons[i]);
            
            // ��������� �������� �� �������
            buttons[i].addActionListener(action);
            
            //���������� ������ ���������
            buttons[i].setEnabled(false);
        }

        // ������ ������ �������� ������: ����� ����, �����, ������ �������
        panelControls.setLayout(new FlowLayout());
        panelControls.add(newB);
        panelControls.add(buttons[0]);
        panelControls.add(quitButton);

        // b[0]  - ����������� ������, �������� ������.
        buttons[0].setText("��������");
        buttons[0].setEnabled(false);

        // ������ ������ ����
        frame.setSize(280, 350);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // ��������� �������� ��� ������ ������ � ����� ����
        newB.addActionListener(action);
        quitButton.addActionListener(action);
    }

    
    /**
     * �������� ���������� ���� � ������� ������ ���� � ������� ��� ���������� �����
     * @return int
     */
     public int selectMode() 
     {
         Object[] options = { "������ � �����������", "������ � ���������",
                 "������" };
         int n = JOptionPane.showOptionDialog(null,
                 "�������� ����� ����:", "����� ����",
                 JOptionPane.YES_NO_CANCEL_OPTION,
                 JOptionPane.INFORMATION_MESSAGE, null, options, options[2]);
         return n;
     }

    //������� � ������� - ���������� ��� ������� � ���������� �� ���
    /**
     * �������� ������ �� ������� �� ������� ����� �������� ���������
     * @param i
     * @return JButton
     */
    public JButton getButtonWithIndex(int i) {
        return buttons[i];
    }
    
    /**
     * �������� ������ ������
     * @return JButton[]
     */
    public JButton[] getButtonArray() {
        return buttons;
    }
    
    /**
     * ���������� ����� ��� �������� ��� ������� (true - ��������)
     * @return boolean
     */
    public boolean getXTurn() {
        return isXturn;
    }
    
    /**
     * ���������� ����������, ���������� �� �� ��� ������ ��� (true  - ��������)
     * @param i
     */
    public void setXTurn(boolean i) {
        isXturn = i;
    }
    
    /**
     * �������� ��������� � ���, ���� �� ������ ���� (false - ���� ����)
     * @return boolean
     */
    public boolean getComplete() {
        return isGameFinished;
    }
    
    /**
     * ���������� ����������, ���������� �� ����� ���� (true - ���� ���������)
     * @param i
     */
    public void setComplete(boolean i) {
        isGameFinished = i;
    }
    
    /**
     * �������� �������� ������� ��������� ������ ��������
     * @return int
     */
    public int getLeftBoxes() {
        return numberOfBoxesLeft;
    }
    
    /**
     * ���������� ������� ��������� ������ ��������
     * @param i
     */
    public void setLeftBoxes(int i) {
        numberOfBoxesLeft = i;
    }
    
    /**
     * ��������� ���-�� ��������� ������ �� 1
     */
    public void reduceLeftBoxes() {
        numberOfBoxesLeft--;
    }
    
    /**
     * �������� ������� � ���� ����� ����� ���� ������ ������������ (0 - ������ ����������, 1 - ������ ������)
     * @return int
     */
    public int getPlayerMode() {
        return playerMode;
    }
    
    /**
     * ���������� ����� ������ (0 - ������ ����������, 1 - ������ ������)
     * @param i
     */
    public void setPlayerMode(int i) {
        playerMode = i;
    }
    
    /**
     * ��� ����������? (��� ���� ������ ����������)
     * @return boolean
     */
    public boolean getIsAITurn() {
        return isAITurn;
    }
    
    /**
     * ���������� ����������, ���������� �� ��� ����� ���������
     * @param turn
     */
    public void setIsAITurn(boolean turn) {
        isAITurn = turn;
    }
}