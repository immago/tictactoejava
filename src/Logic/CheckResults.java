package Logic;
import javax.swing.*;

/**
 * CheckResults
 * ������ ����� ����������� ��������� ������ ���� � ������ ������ � ������ ��� ��� ���� �������
 * @author Dmitry Byankin
 */
public class CheckResults 
{
	//��������������� ����������, �������� �������� � ��� ����������� �� ����
    private boolean resultGameFinished; 
    
    //������ � ������� �����
    private JButton resultB[];

    /**
     * ����������� ������ ������� ���������: 
     * @param getIsGameFinished true, ���� �� ������� �������� ���� ��� �������������
     * @param getB ������� ���� - ������ � ��������
     */
    public CheckResults(boolean getIsGameFinished, JButton getB[]) 
    {
        resultB = getB;
        resultGameFinished = getIsGameFinished;
    }

    /**
     * checkWin
     * ����� �������� ���� ��������� �������� ������
     * @return boolean
     */
    public boolean checkWin() 
    {
        // �������� �� ��������� (1 - 5 - 9)
        checkWinForPositions(1, 5, 9);
        
        // �������� �� �������� ��������� (3 - 5 - 7)
        checkWinForPositions(3, 5, 7);
        
    	
        for (int i = 1; i <= 7; i++) 
        {
            if (resultGameFinished == false) 
            {
            	// �������� �� ��������
                if (i <= 3) 
                	checkWinForPositions(i, i + 3, i + 6);
                       
                // �������� �� �������
                if (i == 1 || i == 4 || i == 7) 
                	checkWinForPositions(i, i + 1, i + 2);   
            }
        }
        return resultGameFinished;
    }

    /**
     * checkWinForPositions
     * ����� �������� �� ������� �������� ������ ������ �� ���� 3� ������� ��������
     * @param x
     * @param y
     * @param z
     */
    public void checkWinForPositions(int x, int y, int z) 
    {
        // �������� ���������
        if (resultB[x].getText().equals("X") && 
        	resultB[y].getText().equals("X") && 
        	resultB[z].getText().equals("X")) 
        {
            declareResult("�������� ��������!"); 
        }
        
        // �������� �������
        if (resultB[x].getText().equals("O") && 
        	resultB[y].getText().equals("O") && 
        	resultB[z].getText().equals("O")) 
        {
            declareResult("������ ��������!");
        }
    }

    /**
     * declareResult
     * �������� ������ � ���������� ��������� �� ������� ���������
     * @param msg
     */
    public void declareResult(String msg) 
    {
        if (resultGameFinished == false) 
        {
        	//���������� ���������
            JOptionPane.showMessageDialog(null, msg);
            
            //��������� ����
            for (int j = 1; j < 10; j++) 
                resultB[j].setEnabled(false);
            
            resultGameFinished = true;
        }
    }

    /**
     * declareDraw
     * �������� �����
     */
    public void declareDraw() 
    {
        JOptionPane.showMessageDialog(null, "�����!");
        resultGameFinished = true;
    }

  
}