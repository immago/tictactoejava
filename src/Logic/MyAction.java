package Logic;
import java.awt.event.*;

import GUI.GUI;

/**
 * ������ ���� ���������� ��������� �� ������� ������ � ������ GUI
 * ������������ ����������� ����� - ActionListener
 * @author Dmitry Byankin
 *
 */
public class MyAction implements ActionListener 
{

    // ��������� ����� GUI ��� ���� ����� �������� ������ � �������.
    private GUI myGUI;

    /**
     * ����������� ������ 
     * @param getLogGUI �������� � ���������� �� ����� ������ GUI, ��� �������� ����� ����������� ��������� �������
     */
    public MyAction(GUI getLogGUI) 
    {
        myGUI = getLogGUI;
    }

    /**
     * ����� ���������� �������, ���������� ����� actionPerformed. ����� ���������� ��� ���������.
     */
    public void actionPerformed(ActionEvent e) 
    {

        CheckResults a = new CheckResults(myGUI.getComplete(), myGUI.getButtonArray());
        AI b = new AI();

        // �������� �� ������ �����
        if (e.getActionCommand() == "�����") 
        {
            System.exit(0);

            // �������� �� ������ ����� ����
        } else if (e.getActionCommand() == "�����") 
        {
            // ����� ������ ����
            myGUI.setPlayerMode(myGUI.selectMode());

            if (myGUI.getPlayerMode() < 2) 
            {
            	//������ ����, ���������� ������
                for (int i = 1; i < 10; i++) 
                {
                    myGUI.getButtonWithIndex(i).setText("");
                    myGUI.getButtonWithIndex(i).setEnabled(true);

                }
                myGUI.setComplete(false);
                myGUI.setLeftBoxes(9);
                
                // ���� playerMode = 0 �� ������ ������ ����������
                if (myGUI.getPlayerMode() == 0) 
                {
                	//���������� ��� ����� ������ (��������)
                    myGUI.setIsAITurn(b.isCPUMovesFirst());

                    // ���� ��� ����������
                    if (myGUI.getIsAITurn()) 
                    {
                        int m = b.moveAI();
                        // ��������� ������ ����� �������� - 'O', 
                        // � ����� ���������� - 'X'
                        //������ 'O', ��������� ������, ��������� ���-�� ��������� ������
                        myGUI.getButtonWithIndex(m).setText("O");
                        myGUI.getButtonWithIndex(m).setEnabled(false);
                        myGUI.reduceLeftBoxes();
                    }
                }
            }

            // �������� ��� ���������� ������ (������ ������)
        } else 
        {
        	//��������� ���������� ��������� ������
            myGUI.reduceLeftBoxes(); 

            //���� ����� (������ ����������)
            if (myGUI.getPlayerMode() == 0) 
            {
                for (int i = 1; i < 10; i++) 
                { 
                	//���������� ������� ������
                    if (e.getSource().equals(myGUI.getButtonWithIndex(i))) 
                    {
                    	//������ � �� �������, ���������, ������ ��� ����������
                        myGUI.getButtonWithIndex(i).setText("X");
                        myGUI.getButtonWithIndex(i).setEnabled(false);
                        myGUI.setXTurn(false);
                    }
                }
                
                //�� �� ������ ������ ���� ����� ������� � ���� ��������� ������ ��������
                myGUI.setComplete(a.checkWin()); 

                // ��� ��
                if (myGUI.getLeftBoxes() != 0 && myGUI.getComplete() == false) 
                {
                    int m;
                    for (;;) 
                    {
                    	//�������� ��� ��, ������� ����� �� ������
                    	//���� ����� �����
                    	//����� �������� ����� ��� ��
                        m = b.moveAI();
                        if (myGUI.getButtonWithIndex(m).getText().equals("")) 
                        {
                            break;
                        }
                    }
                    //�����
                    myGUI.getButtonWithIndex(m).setText("O");
                    myGUI.getButtonWithIndex(m).setEnabled(false);
                    myGUI.reduceLeftBoxes();
                }
                	
            } else if (myGUI.getPlayerMode() == 1) 
            { 
            	//���� ����� 2 ������
            	
            	//���� ������� �������
                for (int i = 1; i < 10; i++) 
                {

                    if (e.getSource().equals(myGUI.getButtonWithIndex(i))) 
                    {
                    	//���� ��� ��������� ������ �������, �������� ���
                        if (myGUI.getXTurn()) 
                        {
                            myGUI.getButtonWithIndex(i).setText("X");
                            myGUI.getButtonWithIndex(i).setEnabled(false);
                            myGUI.getButtonWithIndex(0).setText("������");
                            myGUI.setXTurn(false);
                        } else if (myGUI.getXTurn() == false) 
                        {
                            myGUI.getButtonWithIndex(i).setText("O");
                            myGUI.getButtonWithIndex(i).setEnabled(false);
                            myGUI.setXTurn(true);
                            myGUI.getButtonWithIndex(0).setText("��������");
                        }
                    }
                }
            }
 
            //��������� ������ ���� ����
            myGUI.setComplete(a.checkWin()); 

            //���� ������ �� �������� �����!
            if (myGUI.getLeftBoxes() == 0 && myGUI.getComplete() == false) 
            {
                a.declareDraw();
            }
        }

    }

}